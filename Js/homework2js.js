"use strict";

// 1) JavaScript имеется восемь типов данных:
// String: представляет строку
// Number: представляет числовое значение
// BigInt: предназначен для представления очень
// больших целых чисел
// Boolean: представляет логическое значение true или false
// Undefined: представляет одно специальное значение - undefined и указывает,
// что значение не установлено
// Null: представляет одно специальное значение -
// null и указывает на отсутствие значения
// Symbol: представляет уникальное
// значение, которое часто применяется для обращения к свойствам сложных
// объектов
// Object: представляет комплексный объект
// Первые семь типов представляют примитивные типы данных. Последний тип - Object представляет
// сложный, комплексный тип данных, который состоит из значений примитивных
// типов или других объектов.

// 2) “==” - это оператор нестрогого соответствия, он проверяет соответствие, не
// обращая внимание на тип данных
// “===” -строгое сравнение без приведения к одному типу.

// 3) Оператор — это специальный символ или выражение для проверки, изменения или
// сложения величин.

let name = prompt("Enter your name");

if (name === null || name === "") {
  alert("Enter your correct name");
  name = prompt("Enter your name", "Name");
  if (name === null || name === "") {
    name = "No name";
    alert("Not name");
  }
}
let age = prompt("Enter your age");

if (age === null || age === "" || isNaN(age)) {
  alert("Enter your correct age");
  age = prompt("Enter your age", 18);
}
if (age === null || age === "" || isNaN(age)) {
  alert("No age");
} else {
  if (age < 18) {
    alert(`You are not allowed to visit this website`);
  } else {
    if (age > 17 && age < 23) {
      let age = confirm(`Are you sure you want to continue?`);
      if (age === true) {
        alert(`Welcome, ` + name);
      } else {
        alert(`You are not allowed to visit this website`);
      }
    }
  }
  if (age > 22) {
    alert(`Welcome, ` + name);
  }
}
